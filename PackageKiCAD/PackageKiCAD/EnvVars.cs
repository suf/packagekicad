﻿/********************************************************************************
 Name:		EnvVars.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 EnvVars - Environment variable processing.
 Placeholder of both operating system and KiCADs own environment variables
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PackageKiCAD
{
    /// <summary>
    /// OS/KiCAD Environment variables
    /// </summary>
    public class EnvVars : Dictionary<string, string>
    {
        private Dictionary<string, string> Settings;
        public EnvVars() : base() { }
        public EnvVars(string SettingsFileName) : base()
        {
            bool InEnvironment = false;
            string line;
            string[] lineArr;
            this.Settings = new Dictionary<string, string>();
            StreamReader commonFile;
            commonFile = new StreamReader(SettingsFileName);
            while ((line = commonFile.ReadLine()) != null)
            {
                if(line.Trim().StartsWith("[") && line.Trim() != "[EnvironmentVariables]")
                {
                    InEnvironment = false;
                }
                if (InEnvironment)
                {
                    lineArr = line.Split('=');
                    if (this.Settings.ContainsKey(lineArr[0]))
                    {
                        this.Settings[lineArr[0]] = lineArr[1];
                    }
                    else
                    {
                        this.Settings.Add(lineArr[0], lineArr[1]);
                    }
                }
                if(line.Trim() == "[EnvironmentVariables]")
                {
                    InEnvironment = true;
                }
            }

        }
        /// <summary>
        /// Gather environment variable
        /// </summary>
        /// <param name="envvar">Environment Variable name</param>
        public void AddSetting(string envvar)
        {
            if (!this.Settings.ContainsKey(envvar))
            {
                this.AddFromOS(envvar);
            }
            if (!this.Settings.ContainsKey(envvar))
            {
                this.AddFromSettings(envvar);
            }
        }
        /// <summary>
        /// Gather environment variable from operating system 
        /// </summary>
        /// <param name="envvar">Environment Variable name</param>
        public void AddFromOS(string envvar)
        {
            string envvalue = Environment.GetEnvironmentVariable(envvar);
            if(envvalue != null)
            {
                this.Add(envvar, envvalue);
            }
        }
        /// <summary>
        /// Gather environment variable from the settings file
        /// </summary>
        /// <param name="envvar">Environment Variable name</param>
        public void AddFromSettings(string envvar)
        {
            if(this.Settings.ContainsKey(envvar))
            {
                if (this.ContainsKey(envvar))
                {
                    this[envvar] = System.Text.RegularExpressions.Regex.Unescape(this.Settings[envvar].Trim());
                }
                else
                {
                    this.Add(envvar, System.Text.RegularExpressions.Regex.Unescape(this.Settings[envvar].Trim()));
                }
            }
        }
        /// <summary>
        /// Substitue string in ${[Variable]} format
        /// </summary>
        /// <param name="text">Text to search for</param>
        /// <returns>Substituted text</returns>
        public string Substitute(string text)
        {
            string substituted = text;
            foreach(string key in this.Keys)
            {
                substituted = substituted.Replace("${" + key + "}", this[key]);
            }
            return substituted;
        }
    }
}
