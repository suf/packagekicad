﻿/********************************************************************************
 Name:		SchematicComponents.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 SchematicComponents - Represent all of the components in the schematic file
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PackageKiCAD
{
    public class SchematicComponents : Dictionary<string, List<SchematicComponentEntry>>
    {
        private bool Changed = false;   // Indicate if anything changed in the object since it loaded from file
        private string FileName;
        public SchematicComponents(string FileName) : base()
        {
            this.FileName = FileName;
        }
        public SchematicComponents(IDictionary<string,List<SchematicComponentEntry>> comp, string FileName) :base(comp)
        {
            this.FileName = FileName;
        }
        /// <summary>
        /// Get component based on full name
        /// </summary>
        /// <param name="FullName">Full Name - [library]:[component]</param>
        /// <returns>Object representing the component</returns>
        public SchematicComponentEntry GetComponent(string FullName)
        {
            string[] tmpArr = FullName.Split(":");
            return GetComponent(tmpArr[0], tmpArr[1]);
        }
        /// <summary>
        /// Get component based on library, component name
        /// </summary>
        /// <param name="lib">library name</param>
        /// <param name="Name">component name</param>
        /// <returns>Object representing the component</returns>
        public SchematicComponentEntry GetComponent(string lib, string Name)
        {
            if (this.ContainsKey(lib))
            {
                foreach (SchematicComponentEntry comp in this[lib])
                {
                    if (comp.SymbolName == Name)
                    {
                        return comp;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Substitute the original component name with the one from the packaged library
        /// </summary>
        /// <param name="library">Original library name</param>
        /// <param name="componenet">Original component name</param>
        /// <param name="substitute">The substituted name</param>
        public void Substitute(string library, int componenet, string substitute)
        {
            this[library][componenet].SubstituteSymbol = substitute;
            this.Changed = true;
        }
        /// <summary>
        /// Save the schematic file
        /// </summary>
        public void Save()
        {
            if (this.Changed)
            {
                string line;
                string[] lineArr;
                bool InComponent = false;
                SchematicComponentEntry CurrComponent;
                FileEx.Backup(this.FileName);
                StreamReader origSchFile = new StreamReader(this.FileName + ".bak");
                StreamWriter newSchFile = new StreamWriter(this.FileName);
                while ((line = origSchFile.ReadLine()) != null)
                {
                    if (line.Trim() == "$Comp")
                    {
                        InComponent = true;
                    }
                    if (line.Trim() == "$EndComp")
                    {
                        InComponent = false;
                    }
                    if (InComponent)
                    {
                        lineArr = line.Split(' ');
                        if (lineArr[0] == "L")
                        {
                            if ((CurrComponent = this.GetComponent(lineArr[1])) != null)
                            {
                                if (CurrComponent.SubstituteSymbol != "" && CurrComponent.SubstituteSymbol != null)
                                {
                                    lineArr[1] = CurrComponent.SubstituteSymbol;
                                    line = String.Join(' ', lineArr);
                                }
                            }
                        }
                    }
                    newSchFile.WriteLine(line);
                }
                newSchFile.Flush();
                newSchFile.Close();
            }

        }
        /// <summary>
        /// Load required information from the schematic file
        /// build up the components collection
        /// </summary>
        public void Load()
        {
            string line;
            string[] lineArr;
            string[] compArr;
            bool InComponent = false;
            StreamReader schFile = new StreamReader(this.FileName);
            while ((line = schFile.ReadLine()) != null)
            {
                // begin of the component
                if (line.Trim() == "$Comp")
                {
                    InComponent = true;
                }
                // end of the component
                if (line.Trim() == "$EndComp")
                {
                    InComponent = false;
                }
                if (InComponent)
                {
                    lineArr = line.Split(' ');
                    // L - library element
                    if (lineArr[0] == "L")
                    {
                        compArr = lineArr[1].Split(':');
                        if (this.ContainsKey(compArr[0]))
                        {
                            // compArr[0] - library name, // compArr[1] - component name
                            if (!this[compArr[0]].Contains(new SchematicComponentEntry(compArr[1])))
                            {
                                this[compArr[0]].Add(new SchematicComponentEntry(compArr[1]));
                            }
                        }
                        else
                        {
                            this.Add(compArr[0], new List<SchematicComponentEntry>());
                            this[compArr[0]].Add(new SchematicComponentEntry(compArr[1]));
                        }
                    }
                    // F 2 - footprint library element (unimplemented)
                }
            }
            return;
        }
    }
}
