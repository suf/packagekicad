﻿/********************************************************************************
 Name:		CommandLine.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 CommandLine - Command line option and value processor
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageKiCAD
{
    public class CommandLine : Dictionary<string, CommandLineArgument>
    {
        public CommandLine()
        {
            shortArgs = new Dictionary<string, CommandLineArgument>();
            positionArgs = new List<CommandLineArgument>();
        }
        private Dictionary<string, CommandLineArgument> shortArgs;
        private List<CommandLineArgument> positionArgs;
        public void Add(CommandLineArgument arg)
        {
            base.Add(arg.LongName, arg);
            if (arg.ShortName != "" && arg.ShortName != null)
            {
                this.shortArgs.Add(arg.ShortName, arg);
            }
            if(arg.type == CommandLineArgumentType.PositionText)
            {
                this.positionArgs.Add(arg);
            }
        }
        
        public void Process(string[] Arguments)
        {
            int i;
            int position = 0;
            for(i = 0; i < Arguments.Length; i++)
            {
                // Longname
                if(Arguments[i].StartsWith("--"))
                {
                    if(this.ContainsKey(Arguments[i].Substring(2)))
                    {
                        switch(this[Arguments[i].Substring(2)].type)
                        {
                            case CommandLineArgumentType.Switch:
                                this[Arguments[i].Substring(2)].ValueBool = true;
                                break;
                            case CommandLineArgumentType.PositionText:
                            case CommandLineArgumentType.Text:
                                if (!Arguments[i + 1].StartsWith("-"))
                                {
                                    this[Arguments[i].Substring(2)].ValueStr = Arguments[i + 1];
                                    i++;
                                }
                                break;
                        }
                    }
                    continue;
                }
                if (Arguments[i].StartsWith("-"))
                {
                    if (this.shortArgs.ContainsKey(Arguments[i].Substring(1)))
                    {
                        switch (this.shortArgs[Arguments[i].Substring(1)].type)
                        {
                            case CommandLineArgumentType.Switch:
                                this.shortArgs[Arguments[i].Substring(1)].ValueBool = true;
                                break;
                            case CommandLineArgumentType.PositionText:
                            case CommandLineArgumentType.Text:
                                if (!Arguments[i + 1].StartsWith("-"))
                                {
                                    this.shortArgs[Arguments[i].Substring(1)].ValueStr = Arguments[i + 1];
                                    i++;
                                }
                                break;
                        }
                    }
                    continue;
                }
                if(position < this.positionArgs.Count)
                {
                    this.positionArgs[position].ValueStr = Arguments[i];
                }
            }
        }
        
    }
    public class CommandLineArgument
    {
        public string ShortName;
        public string LongName;
        public CommandLineArgumentType type;

        public CommandLineArgument(CommandLineArgumentType type, string LongName, string ShortName)
        {
            this.type = type;
            this.LongName = LongName;
            this.ShortName = ShortName;
        }
        public CommandLineArgument(string LongName)
        {
            this.type = CommandLineArgumentType.PositionText;
            this.LongName = LongName;
            this.ShortName = "";
        }

        public string ValueStr = "";
        public bool ValueBool = false;
    }
    public enum CommandLineArgumentType { PositionText, Text, Switch }
}
