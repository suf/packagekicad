﻿/********************************************************************************
 Name:		SymLib.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 SymLib - Represent a Symbol library
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PackageKiCAD
{
    /// <summary>
    /// Representation of the Symbolic library and Dcm file
    /// </summary>
    public class SymLib
    {
        bool FileChanged;
        public string libFileName;
        string dcmFileName;
        public string libname;
        public SymLib()
        {
            this.symLibEntries = new Dictionary<string, SymLibEntry>();
            this.symLibEntryNames = new List<string>();
            this.FileChanged = true; // Newly created file

        }
        private Dictionary<string, SymLibEntry> symLibEntries; // Dictionary of all entries. All of the
                                                               // component names/aliasses used as keys
                                                               // all of the components are exists only
                                                               // once
        private List<string> symLibEntryNames; // doesn't contain the Aliases. Only required for render
        public SymLib(string path, string libname)
        {
            libFileName = Path.Combine(path, libname + ".lib");
            dcmFileName = Path.Combine(path, libname + ".dcm");
            this.symLibEntries = new Dictionary<string, SymLibEntry>();
            this.symLibEntryNames = new List<string>();
            this.FileChanged = true; // Newly created file
            this.libname = libname;
        }
        public SymLib(string libFileName)
        {
            this.libFileName = libFileName;
            this.libname = Path.GetFileNameWithoutExtension(libFileName);
            this.dcmFileName = Path.Combine(Path.GetDirectoryName(libFileName), this.libname + ".dcm");
            this.symLibEntries = new Dictionary<string, SymLibEntry>();
            this.symLibEntryNames = new List<string>();
            this.FileChanged = true; // Newly created file
        }
        /// <summary>
        /// Load the lib and dcm file from the disk
        /// </summary>
        public void LoadFromFile()
        {
            bool InComponent = false;
            StreamReader libFile;
            StreamReader dcmFile;
            string line;
            string[] lineArr;
            SymLibEntry CurrentLibEntry = null;
            this.FileChanged = true; // Newly created file
            // Process libraryfile
            if (File.Exists(this.libFileName))
            {
                this.FileChanged = false; // Loading from file, means at the end of the process the file on the disk and in the memory is the same
                libFile = new StreamReader(this.libFileName);
                while ((line = libFile.ReadLine()) != null)
                {
                    lineArr = line.Split(' ');
                    switch (lineArr[0])
                    {
                        case "DEF":
                            if(!this.symLibEntryNames.Contains(lineArr[1]))
                            {
                                this.symLibEntryNames.Add(lineArr[1]);
                            }
                            if (this.symLibEntries.ContainsKey(lineArr[1]))
                            {
                                CurrentLibEntry = this.symLibEntries[lineArr[1]];
                            }
                            else
                            {
                                CurrentLibEntry = new SymLibEntry();
                                this.symLibEntries.Add(lineArr[1], CurrentLibEntry);
                            }
                            CurrentLibEntry.Name = lineArr[1];
                            if (!CurrentLibEntry.Names.Contains(lineArr[1]))
                            {
                                CurrentLibEntry.Names.Add(lineArr[1]);
                            }
                            CurrentLibEntry.libcontent = "#\r\n# " + lineArr[1] + "\r\n#\r\n";
                            InComponent = true;
                            break;
                        case "ALIAS":
                            for (int i = 1; i < lineArr.Length; i++)
                            {
                                if(CurrentLibEntry != null && !CurrentLibEntry.Names.Contains(lineArr[i]))
                                {
                                    CurrentLibEntry.Names.Add(lineArr[i]);
                                }
                                if(!this.symLibEntries.ContainsKey(lineArr[i]))
                                {
                                    this.symLibEntries.Add(lineArr[i], CurrentLibEntry);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    if(InComponent)
                    {
                        CurrentLibEntry.libcontent += line + "\r\n";
                        InComponent = lineArr[0] != "ENDDEF";
                    }
                }
                libFile.Close();
            }
            // Process DCM file
            string CurrentName = "";
            if (File.Exists(this.dcmFileName))
            {
                this.FileChanged = false; // Loading from file, means at the end of the process the file on the disk and in the memory is the same
                dcmFile = new StreamReader(this.dcmFileName);
                while ((line = dcmFile.ReadLine()) != null)
                {
                    lineArr = line.Split(' ');
                    if(lineArr[0] == "$CMP")
                    { 
                        CurrentName = lineArr[1];
                        if (this.symLibEntries.ContainsKey(CurrentName))
                        {
                            CurrentLibEntry = this.symLibEntries[CurrentName];
                            if(!CurrentLibEntry.dcmcontent.ContainsKey(CurrentName))
                            {
                                CurrentLibEntry.dcmcontent.Add(CurrentName, "#\r\n");
                            }
                            InComponent = true;
                        }
                        else
                        {
                            InComponent = false;
                        }
                    }
                    if (InComponent)
                    {
                        CurrentLibEntry.dcmcontent[CurrentName] += line + "\r\n";
                        InComponent = lineArr[0] != "$ENDCMP";
                    }
                }
                dcmFile.Close();
            }

        }
        /// <summary>
        /// Add entry into the library
        /// </summary>
        /// <param name="component">Component to add</param>
        public void Add(SymLibEntry component)
        {
            if (!this.symLibEntryNames.Contains(component.Name))
            {
                this.FileChanged = true; // Successfully adding a component
                this.symLibEntryNames.Add(component.Name);
                foreach (string name in component.Names)
                {
                    if (!this.symLibEntries.ContainsKey(name))
                    {
                        this.symLibEntries.Add(name, component);
                    }
                    else
                    {
                        // Collision
                    }
                }
            }
            else
            {
                if (!component.Equals(this[component.Name]))
                {
                    // Collision handling!!!
                }
            }
        }
        /// <summary>
        /// Add a new alias to a component
        /// </summary>
        /// <param name="name">Name of the component</param>
        /// <param name="alias">New alias to add</param>
        public void AddAlias(string name, string alias)
        {
            if(this.symLibEntries.ContainsKey(name))
            {
                this.symLibEntries[name].AddAlias(alias);
                this.symLibEntries.Add(alias, this.symLibEntries[name]);
            }
        }
        /// <summary>
        /// Get a component based on name or alias
        /// </summary>
        /// <param name="key">Name or alias</param>
        /// <returns>The component</returns>
        public SymLibEntry this[string key]
        {
            get
            {
                return this.symLibEntries[key];
            }
        }
        /// <summary>
        /// Determines whether the library contains the specified component
        /// </summary>
        /// <param name="name">Name or alias of the component</param>
        /// <returns>true if the component exists</returns>
        public bool ContainsComponent(string name)
        {
            return this.symLibEntries.ContainsKey(name);
        }
        /// <summary>
        /// Save the library and DCM file
        /// </summary>
        public void Save()
        {
            if (this.FileChanged)
            {
                FileEx.Backup(this.libFileName);
                FileEx.Backup(this.dcmFileName);
                StreamWriter libFile = new StreamWriter(this.libFileName);
                StreamWriter dcmFile = new StreamWriter(this.dcmFileName);
                libFile.Write("EESchema-LIBRARY Version 2.4\r\n#encoding utf-8\r\n");
                dcmFile.Write("EESchema - DOCLIB  Version 2.\r\n");
                foreach (string key in this.symLibEntryNames)
                {
                    libFile.Write(this.symLibEntries[key].libcontent);
                    foreach (string dcmvalue in this.symLibEntries[key].dcmcontent.Values)
                    {
                        dcmFile.Write(dcmvalue);
                    }
                }
                libFile.Flush();
                libFile.Close();
                dcmFile.Write("#End Doc Library");
                dcmFile.Flush();
                dcmFile.Close();
            }
        }

    }
}
