﻿/********************************************************************************
 Name:		Global.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 Global - Global variables
 Placeholder of both operating system and KiCADs own environment variables
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace PackageKiCAD
{
    /// <summary>
    /// Global variables
    /// </summary>
    public sealed class Global
    {
        private static readonly Lazy<Global>
            lazy =
            new Lazy<Global>
                (() => new Global());

        public static Global Instance { get { return lazy.Value; } }

        private Global()
        {
            if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                this.OS = Platform.Windows;
                this.CS = false;
                return;
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                this.OS = Platform.Linux;
                this.CS = true;
                return;
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
            {
                this.OS = Platform.OSX;
                this.CS = false;
                return;
            }
        }
        /// <summary>
        /// The operating system this code is running on
        /// </summary>
        public Platform OS;
        /// <summary>
        /// File System Case Sensitivity
        /// </summary>
        public bool CS;
    }
    /// <summary>
    /// Operating System Platform types
    /// </summary>
    public enum Platform { Windows, Linux, OSX }
}
