﻿/********************************************************************************
 Name:		FileEx.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 FileEx - Extended File handling static functions
 Placeholder of both operating system and KiCADs own environment variables
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PackageKiCAD
{
    public static class FileEx
    {
        /// <summary>
        /// Create a file backup: move to .bak file
        /// </summary>
        /// <param name="FileName">Original file name</param>
        public static void Backup(string FileName)
        {
            FileEx.Backup(FileName, "bak", true);
        }
        /// <summary>
        /// Create a file backup: move to .[Extension] file
        /// </summary>
        /// <param name="FileName">Original file name</param>
        /// <param name="Extension">Backup extension</param>
        public static void Backup(string FileName, string Extension)
        {
            FileEx.Backup(FileName, Extension, true);
        }
        /// <summary>
        /// Create a file backup: move/copy to .bak file
        /// </summary>
        /// <param name="FileName">Original file name</param>
        /// <param name="Move">Operation type - true: move, false: copy</param>
        public static void Backup(string FileName, bool Move)
        {
            FileEx.Backup(FileName, "bak", Move);
        }
        /// <summary>
        /// Create a file backup: move/copy to .[Extension] file
        /// </summary>
        /// <param name="FileName">Original file name</param>
        /// <param name="Extension">Backup extension</param>
        /// <param name="Move">Operation type - true: move, false: copy</param>
        public static void Backup(string FileName, string Extension, bool Move)
        {
            if (File.Exists(FileName))
            {
                if (File.Exists(FileName + "." + Extension))
                {
                    File.Delete(FileName + "." + Extension);
                }
                if (Move)
                {
                    File.Move(FileName, FileName + "." + Extension);
                }
                else
                {
                    File.Copy(FileName, FileName + "." + Extension);
                }
            }
        }
        /// <summary>
        /// Normalize slash and backslash characters in the path string according
        /// to the current OS Platform
        /// </summary>
        /// <param name="Path">Path string</param>
        /// <returns>Normalized path string</returns>
        public static string NormalizePath(string Path)
        {
            Global global = Global.Instance;
            return NormalizePath(Path, global.OS);
        }
        /// <summary>
        /// Normalize slash and backslash characters in the path string according
        /// to the given OS Platform
        /// </summary>
        /// <param name="Path">Path string</param>
        /// <param name="platform">OS Platform</param>
        /// <returns>Normalized path string</returns>
        public static string NormalizePath(string Path, Platform platform)
        {
            switch(platform)
            {
                case Platform.Windows:
                    return Path.Replace('/', '\\');
                case Platform.OSX:
                case Platform.Linux:
                    return Path.Replace('\\', '/');
            }
            return Path;
        }
    }
}
