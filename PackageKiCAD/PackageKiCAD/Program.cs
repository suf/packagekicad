﻿/********************************************************************************
 Name:		Program.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 Program - The main entry point
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/

using System;
using System.IO;
using System.Collections.Generic;

namespace PackageKiCAD
{
    class Program
    {
        static void Main(string[] args)
        {
            // variables

            // Global
            string KicadAppdata = "";   // location of the global files used by KiCAD
            EnvVars SymLibVars;         // Environment variables for symbol libraries
            string SymLibGlobalFile = "";
            ProjectProcessor processor;

            // Working
            CommandLine cl = new CommandLine();

            // Greating message
            Console.WriteLine("PackageKiCAD v0.2Beta - KiCAD project packager");
            Console.WriteLine("Written by SUF. Distributed under GPLv3 license.");

            // Geather environment
            switch (Global.Instance.OS)
            {
                case Platform.Windows:
                    KicadAppdata = Path.Combine(Environment.GetEnvironmentVariable("APPDATA"), "kicad");
                    break;
                case Platform.Linux:
                    KicadAppdata = Path.Combine(Environment.GetEnvironmentVariable("HOME"), ".config/kicad");
                    break;
            }
            SymLibVars = new EnvVars(Path.Combine(KicadAppdata, "kicad_common"));
            SymLibVars.AddSetting("KICAD_SYMBOL_DIR");

            // Geather global settings
            SymLibGlobalFile = Path.Combine(KicadAppdata, "sym-lib-table");
            if (!File.Exists(SymLibGlobalFile))
            {
                SymLibGlobalFile = "";
            }

            processor = new ProjectProcessor(SymLibVars, SymLibGlobalFile, cl);

            // Process command line
            cl.Add(new CommandLineArgument("projectfile"));
            // cl.Add(new CommandLineArgument(CommandLineArgumentType.Switch, "symlibcopy", "sc"));
            // cl.Add(new CommandLineArgument(CommandLineArgumentType.Switch, "symlibpackage", "sp"));
            // cl.Add(new CommandLineArgument(CommandLineArgumentType.Switch, "disablebackup", "db"));
            cl.Add(new CommandLineArgument(CommandLineArgumentType.Switch, "Recursive", "R"));
            // cl.Add(new CommandLineArgument(CommandLineArgumentType.Switch, "gitignore", "g"));
            // cl.Add(new CommandLineArgument(CommandLineArgumentType.Switch, "validate", "v"));
            // Clear original local library list, just keep the packaged
            cl.Add(new CommandLineArgument(CommandLineArgumentType.Switch, "clearlib", "cl"));
            cl.Process(args);

            if(cl["projectfile"].ValueStr != "" && cl["projectfile"].ValueStr != null)
            {
                // work from the given project file or from the given directory
                if(File.Exists(cl["projectfile"].ValueStr))
                {
                    // File
                    processor.Process(cl["projectfile"].ValueStr);
                }
                if(Directory.Exists(cl["projectfile"].ValueStr))
                {
                    // Directory
                    foreach(string ProjectFileName in Directory.EnumerateFiles(cl["projectfile"].ValueStr, "*.pro",cl["Recursive"].ValueBool ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                    {
                        processor.Process(ProjectFileName);
                    }
                }
            }
            else
            {
                // work from current directory
                foreach (string ProjectFileName in Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "*.pro", cl["Recursive"].ValueBool ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                {
                    processor.Process(ProjectFileName);
                }
            }
        }
    }
}
