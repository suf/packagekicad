﻿/********************************************************************************
 Name:		SymLibTable.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 SymLibTable - Represent a Symbol library table (local or global)
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Xml.Linq;
using System.IO;

namespace PackageKiCAD
{
    public class SymLibTable:IndexedDictionary<string,SymLibTableEntry>
    {
        private string FileName;
        public SymLibTable(string FileName) : base()
        {
            this.FileName = FileName;
        }
        
        public SymLibTable(IDictionary<string,SymLibTableEntry> dict, string FileName) : base(dict)
        {
            this.FileName = FileName;
        }
        /// <summary>
        /// Save to file
        /// </summary>
        /// <param name="env">Environment variable table. Used for compress uri with the environment variables (for relative path)</param>
        public void Save(EnvVars env)
        {
            this.Save(env, this.Count);
        }
        /// <summary>
        /// Save to file
        /// </summary>
        /// <param name="env">Environment variable table. Used for compress uri with the environment variables (for relative path)</param>
        /// <param name="Count">Number of elements to save from the list</param>
        public void Save(EnvVars env, int Count)
        {
            // Handle if it not changed - Implementation required
            FileEx.Backup(this.FileName);
            StreamWriter symlibtable = new StreamWriter(this.FileName);
            symlibtable.WriteLine("(sym_lib_table");
            for(int i = 0; i < Count; i++)
            {
                symlibtable.WriteLine("  " + this[i].Value.Render(env));
            }
            symlibtable.WriteLine(")");
            symlibtable.Flush();
            symlibtable.Close();
        }
        /// <summary>
        /// Load table from file
        /// </summary>
        /// <param name="FileName">Name of the file</param>
        /// <returns></returns>
        public static SymLibTable LoadFromFile(string FileName)
        {
            // Read the complette table from file
            string content = File.ReadAllText(FileName);
            // Convert it to XML
            string xmlLibText = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
            bool InQuote = false;
            bool InTag = false;
            string Tag = "";
            Stack<string> Tags = new Stack<string>();
            for (int i = 0; i < content.Length; i++)
            {
                switch (content[i])
                {
                    case ' ':
                    case '\t':
                    case '\n':
                    case '\r':
                        if (InQuote)
                        {
                            xmlLibText += content[i];
                        }
                        else
                        {
                            if (InTag)
                            {
                                Tags.Push(Tag);
                                Tag = "";
                                xmlLibText += '>';
                                InTag = false;
                            }
                        }
                        break;
                    case '\"':
                        InQuote = !InQuote;
                        break;
                    case '(':
                        if (InQuote)
                        {
                            xmlLibText += '(';
                        }
                        else
                        {
                            if (InTag)
                            {
                                Tags.Push(Tag);
                                Tag = "";
                                xmlLibText += '>';
                            }
                            xmlLibText += '<';
                            InTag = true;
                        }
                        break;
                    case ')':
                        if (InTag && !InQuote)
                        {
                            InTag = false;
                            Tag = "";
                            xmlLibText += " />";
                        }
                        else
                        {
                            xmlLibText += InQuote ? ")" : "</" + Tags.Pop() + ">";
                        }
                        break;
                    default:
                        xmlLibText += content[i];
                        if (InTag)
                        {
                            Tag += content[i];
                        }
                        break;
                }
            }
            // Parse the XML to a Dictionary of table entries
            XDocument xdoc = XDocument.Parse(xmlLibText);
            return new SymLibTable(xdoc.Descendants("lib").ToDictionary(
                lib => lib.Descendants("name").FirstOrDefault().Value,
                lib => new SymLibTableEntry
                {
                    Description = lib.Descendants("descr").FirstOrDefault().Value,
                    LibType = lib.Descendants("type").FirstOrDefault().Value,
                    Name = lib.Descendants("name").FirstOrDefault().Value,
                    Options = lib.Descendants("options").FirstOrDefault().Value,
                    Uri = lib.Descendants("uri").FirstOrDefault().Value,
                    Enabled = !lib.Descendants("disabled").Any()
                }), FileName);

        }
    }
}
