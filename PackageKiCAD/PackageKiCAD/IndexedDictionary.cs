﻿/********************************************************************************
 Name:		IndexedDictionary.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 IndexedDictionary - Generic dictionary can be accessed by key or numeric index.
 Can be used to store sorted elements
 Placeholder of both operating system and KiCADs own environment variables
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageKiCAD
{
    /// <summary>
    /// Dictionary with key based and numeric index
    /// </summary>
    /// <typeparam name="T">Key type</typeparam>
    /// <typeparam name="K">Value type</typeparam>
    public class IndexedDictionary<T, K> : Dictionary<T, K>
    {
        private List<T> IndexList; // store the keys in a numeric indexed list
        public IndexedDictionary()
        {
            this.IndexList = new List<T>();
        }
        /// <summary>
        /// Constructor - Build an IndexedDictionary from a regular Dictionary
        /// </summary>
        /// <param name="dict">The original Dictionary object</param>
        public IndexedDictionary(IDictionary<T,K> dict) : base(dict)
        {
            this.IndexList = new List<T>();
            foreach (T key in dict.Keys)
            {
                this.IndexList.Add(key);
            }
        }
        /// <summary>
        /// Numeric index
        /// </summary>
        /// <param name="index"></param>
        /// <returns>Key/value pair from the base dictionary</returns>
        public KeyValuePair<T,K> this[int index]
        {
            get
            {
                return new KeyValuePair<T, K>(this.IndexList[index], base[this.IndexList[index]]);
            }
        }
        /// <summary>
        /// Add key/value pair into the dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public new void Add(T key, K value)
        {
            base.Add(key, value);
            this.IndexList.Add(key);
        }
        /// <summary>
        /// Insert key/value pair into the dictionary
        /// to the first position (if accessed by numeric index)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void InsertBegining(T key, K value)
        {
            base.Add(key, value);
            this.IndexList.Insert(0, key);
        }
        /// <summary>
        /// Insert key/value pair into the dictionary
        /// to the given position (if accessed by numeric index)
        /// </summary>
        /// <param name="index">the position where the key/value pair inserted</param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Insert(int index, T key, K value)
        {
            base.Add(key, value);
            this.IndexList.Insert(index, key);
        }
        /// <summary>
        /// Change order of the existing elements in the dictionary
        /// </summary>
        /// <param name="oldindex">Original index of the key/value pair</param>
        /// <param name="newindex">Resulting index of the key/value pair</param>
        public void ChangeIndex(int oldindex, int newindex)
        {
            if (oldindex != newindex)
            {
                T tmpValue = this.IndexList[oldindex];
                this.IndexList.RemoveAt(oldindex);
                this.IndexList.Insert(newindex, tmpValue);
            }
        }
        /// <summary>
        /// Get the numeric index of a key on the list
        /// </summary>
        /// <param name="key">Key to find</param>
        /// <returns>Index of the key</returns>
        public int GetIndex(T key)
        {
            int index;
            bool found = false;
            for(index = 0; index < this.IndexList.Count; index++)
            {
                if(this.IndexList[index].Equals(key))
                {
                    found = true;
                    break;
                }
            }
            return found?index:-1;
        }
    }
}
