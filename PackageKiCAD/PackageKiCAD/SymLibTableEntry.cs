﻿/********************************************************************************
 Name:		SymLibTableEntry.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 SymLibTableEntry - Represent a Symbol library in a Symbol library table (local or global)
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageKiCAD
{
    public class SymLibTableEntry
    {
        public SymLibTableEntry() {}
        public SymLibTableEntry(SymLib lib)
        {
            this.Name = lib.libname;
            this.LibType = "legacy";
            this.Uri = lib.libFileName;
            this.Options = "";
            this.Description = "";
            this.Enabled = true;
        }
        public string Name { get; set; }
        public string LibType { get; set; }
        public string Uri { get; set; }
        public string Options { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        /// <summary>
        /// Render a line in the symbol library table
        /// </summary>
        /// <param name="env">Environment variabled for Uri path compression</param>
        /// <returns></returns>
        public string Render(EnvVars env)
        {
            string uri = FileEx.NormalizePath(this.Uri, Platform.Linux);
            string envvalue;
            if (env != null)
            {
                foreach (string key in env.Keys)
                {
                    envvalue = FileEx.NormalizePath(env[key], Platform.Linux);
                    uri = uri.Replace(envvalue, "${" + key + "}");
                }
            }
            return "(lib" +
                "(name " + this.Name + ")" +
                "(type " + this.LibType + ")" +
                (uri.IndexOf(' ') < 0 ? "(uri " : "(uri \"") + uri + (uri.IndexOf(' ') < 0 ? ")" : "\")") +
                "(options \"" + this.Options + "\")" +
                "(descr \"" + this.Description + "\")" +
                (this.Enabled ? "" : "(disabled)") + ")";
        }
    }
}
