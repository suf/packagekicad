﻿/********************************************************************************
 Name:		SchematicComponentEntry.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 SchematicComponentEntry - Represent a component in the schematic file
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageKiCAD
{
    public class SchematicComponentEntry : IEquatable<SchematicComponentEntry>
    {
        public SchematicComponentEntry() { }
        public SchematicComponentEntry(string symbolName)
        {
            this.SymbolName = symbolName;
        }
        /// <summary>
        /// Name of the symbol inside the library
        /// </summary>
        public string SymbolName { get; set; }
        /// <summary>
        /// String used for substitution at rendering
        /// Contains the original symbol name plus the original library name
        /// </summary>
        public string SubstituteSymbol;

        /// <summary>
        /// Compare to an other object. It is equal if the name is the same
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(SchematicComponentEntry other)
        {
            return this.SymbolName == other.SymbolName;
        }
    }
}
