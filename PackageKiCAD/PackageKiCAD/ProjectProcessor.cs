﻿/********************************************************************************
 Name:		ProjectProcessor.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 ProjectProcessor - The worker process - process the KiCAD projects
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PackageKiCAD
{
    public class ProjectProcessor
    {
        EnvVars env;
        SymLibTable SymLibTblGlobal;
        CommandLine Options;
        public ProjectProcessor(EnvVars env, string SymLibGlobalFile, CommandLine Options)
        {
            this.env = env;
            this.Options = Options;
            // Global
            if (SymLibGlobalFile != "")
            {
                SymLibTblGlobal = SymLibTable.LoadFromFile(SymLibGlobalFile);
            }
        }
        public void Process(string fileName)
        {
            // Project (local)
            string ProjectDir = "";
            string ProjectName = "";
            string SchematicFile = "";
            string SymLibLocalFile = "";
            SymLib SymLibPackaged = null;
            SymLibTable SymLibTblLocal = null;
            SchematicComponents SchComp = null;

            // Working
            SymLib SymLibCurrent = null;
            string CompPackedName = "";
            bool SymLibFound = false;

            ProjectDir = Path.GetDirectoryName(fileName);
            ProjectName = Path.GetFileNameWithoutExtension(fileName);
            SchematicFile = Path.Combine(ProjectDir, ProjectName + ".sch");
            if (!File.Exists(SchematicFile))
            {
                SchematicFile = "";
            }
            SymLibLocalFile = Path.Combine(ProjectDir, "sym-lib-table");
            if (!File.Exists(SymLibLocalFile))
            {
                SymLibLocalFile = "";
            }
            if (ProjectDir != "")
            {
                if (this.env.ContainsKey("KIPRJMOD"))
                {
                    this.env["KIPRJMOD"] = ProjectDir;
                }
                else
                {
                    this.env.Add("KIPRJMOD", ProjectDir);
                }
            }

            if (SymLibLocalFile != "")
            {
                SymLibTblLocal = SymLibTable.LoadFromFile(SymLibLocalFile);
                for (int i = 0; i < SymLibTblLocal.Count; i++)
                {
                    // If the package library found - already packed previously
                    if (SymLibTblLocal[i].Key == ProjectName + "-packed")
                    {
                        // If the package library is not on the top of the list - move
                        if (i != 0)
                        {
                            SymLibTblLocal.ChangeIndex(i, 0);
                        }
                    }
                }
            }
            // Load packaged library
            if (ProjectDir != "" && ProjectName != "")
            {
                SymLibPackaged = new SymLib(ProjectDir, ProjectName + "-packed");
                SymLibPackaged.LoadFromFile();
                // If the library is not on the local library list, add to it
                if (!SymLibTblLocal.ContainsKey(ProjectName + "-packed"))
                {
                    SymLibTblLocal.InsertBegining(ProjectName + "-packed", new SymLibTableEntry(SymLibPackaged));
                }
            }
            // Load currently used components from the schematic file
            if (SchematicFile != "")
            {
                SchComp = new SchematicComponents(SchematicFile);
                SchComp.Load();
            }
            // Iterate the components
            foreach (string libname in SchComp.Keys)
            {
                SymLibFound = false;
                // Load the library determined by the key into the memory
                // except the packaged library, as
                // 1. It is already loaded
                // Need a different care
                if (libname != (ProjectName + "-packed"))
                {
                    if (SymLibTblLocal != null)
                    {
                        if (SymLibTblLocal.ContainsKey(libname))
                        {
                            SymLibCurrent = new SymLib(this.env.Substitute(SymLibTblLocal[libname].Uri));
                            SymLibCurrent.LoadFromFile();
                            SymLibFound = true;
                        }
                    }
                    if (SymLibTblGlobal != null && !SymLibFound)
                    {
                        if (SymLibTblGlobal.ContainsKey(libname))
                        {
                            SymLibCurrent = new SymLib(this.env.Substitute(SymLibTblGlobal[libname].Uri));
                            SymLibCurrent.LoadFromFile();
                            SymLibFound = true;
                        }
                    }
                    // On every component from the same library in the schematic design
                    for (int i = 0; i < SchComp[libname].Count; i++)
                    {
                        // Generate the packaged name of the component
                        CompPackedName = SchComp[libname][i].SymbolName + Constants.LibSeparator + libname;
                        // If this is already in the packaged library, just substitute with the packaged name
                        if (SymLibPackaged.ContainsComponent(CompPackedName))
                        {
                            SchComp.Substitute(libname, i, SymLibPackaged.libname + ":" + CompPackedName);
                        }
                        else
                        {
                            // we found the library
                            if (SymLibFound)
                            {
                                // we found the component
                                if (SymLibCurrent.ContainsComponent(SchComp[libname][i].SymbolName))
                                {
                                    // add to the packaged library
                                    SymLibPackaged.Add(SymLibCurrent[SchComp[libname][i].SymbolName]);
                                    // add the packaged name as alias
                                    SymLibPackaged.AddAlias(SchComp[libname][i].SymbolName, CompPackedName);
                                    // add the substisution name to the SchComp object
                                    SchComp.Substitute(libname, i, SymLibPackaged.libname + ":" + CompPackedName);
                                }
                                else
                                {
                                    Console.WriteLine(libname + ":" + SchComp[libname][i].SymbolName + " - Missing");
                                }
                            }
                            else
                            {
                                Console.WriteLine(libname + ":" + SchComp[libname][i].SymbolName + " - Missing");
                            }
                        }
                    }
                }
                else
                {
                    // All of the components need to be in the lib already, just check it and report
                    // if something missing
                    for (int i = 0; i < SchComp[libname].Count; i++)
                    {
                        if (!SymLibPackaged.ContainsComponent(SchComp[libname][i].SymbolName))
                        {
                            Console.WriteLine(libname + ":" + SchComp[libname][i].SymbolName + " - Missing");
                        }
                    }
                }
            }
            // Render the library
            if (ProjectDir != "" && ProjectName != "")
            {
                // If the library is not on the local library list, add to it
                // !!! Later add an option to keep only the packaged library on the list !!!
                if (SymLibTblLocal.ContainsKey(ProjectName + "-packed"))
                {
                    SymLibTblLocal.ChangeIndex(SymLibTblLocal.GetIndex(ProjectName + "-packed"), 0);
                }
                else
                { 
                    SymLibTblLocal.InsertBegining(ProjectName + "-packed", new SymLibTableEntry(SymLibPackaged));
                }
                if (this.Options["clearlib"].ValueBool)
                {
                    SymLibTblLocal.Save(env,1);
                }
                else
                {
                    SymLibTblLocal.Save(env);
                }
            }
            SymLibPackaged.Save();
            // Render the schematic
            SchComp.Save();
        }
    }
}
