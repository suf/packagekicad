﻿/********************************************************************************
 Name:		SymLibEntry.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 SymLibEntry - Represent a Symbol library component
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageKiCAD
{
    public class SymLibEntry : IEquatable<SymLibEntry>
    {
        public SymLibEntry()
        {
            this.Names = new List<string>();
            this.dcmcontent = new Dictionary<string, string>();
        }
        /// <summary>
        /// Add alias to the component
        /// </summary>
        /// <param name="alias">Alias name</param>
        /// <param name="dcmcontent">Content of the dcm file connecting to the alias</param>
        internal void AddAlias(string alias, string dcmcontent)
        {
            this.AddAlias(alias);
            if (dcmcontent != "" && dcmcontent != null)
            {
                this.dcmcontent.Add(alias, dcmcontent);
            }
        }
        /// <summary>
        /// Add alias to the component
        /// </summary>
        /// <param name="alias">Content of the dcm file connecting to the alias</param>
        internal void AddAlias(string alias)
        {
            string[] tmpContentArr = this.libcontent.Split("\r\n");
            this.Names.Add(alias);
            this.libcontent = "";
            bool AliasInserted = false;
            // Find ALIAS
            for (int i = 0; i < tmpContentArr.Length; i++)
            {
                if (tmpContentArr[i].Split(" ")[0] == "ALIAS")
                {
                    tmpContentArr[i] += " " + alias;
                    AliasInserted = true;
                    break;
                }
            }
            // Find $FPLIST or DRAW
            for (int i = 0; (i < tmpContentArr.Length) && !AliasInserted; i++)
            {
                if (tmpContentArr[i].Split(" ")[0] == "$FPLIST" || tmpContentArr[i].Split(" ")[0] == "DRAW")
                {
                    tmpContentArr[i] = "ALIAS " + alias + "\r\n" + tmpContentArr[i];
                    AliasInserted = true;
                    break;
                }
            }
            // If AliasInserted is still false signal error
            if (!AliasInserted)
            {
                Console.WriteLine("Error: Unable to add alias: " + alias);
            }
            // process
            this.libcontent = String.Join("\r\n", tmpContentArr) + "\r\n";
        }
        /// <summary>
        /// Determines whether the component is equal to another
        /// </summary>
        /// <param name="other">The other component</param>
        /// <returns></returns>
        public bool Equals(SymLibEntry other)
        {
            // The two components are considered equal if the library content is equal
            if (other == null)
            {
                return false;
            }
            return String.Equals(this.libcontent, other.libcontent);
        }

        public List<string> Names; // list of the aliases + name
        public string Name; // Name of the component
        public string libcontent; // content of the component in the library file
        public Dictionary<string, string> dcmcontent; // dcm content can be defined by alias, so not only one can exists by one lib entry
    }
}
