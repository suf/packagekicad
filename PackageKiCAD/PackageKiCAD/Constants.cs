﻿/********************************************************************************
 Name:		Constants.cs
 Author:	SUF
 Description:
 Packaging tool for KiCAD projects - used for source controll and sharing
 Constants - Object for various used constants
 Reference:

 License:
 This work is licensed under a GNU General Public License v3
 https://www.gnu.org/licenses/gpl-3.0.en.html
 ********************************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace PackageKiCAD
{
    public static class Constants
    {
        /// <summary>
        /// Used as separator between the component and the original library name in the packaged library
        /// </summary>
        public const string LibSeparator = "++";
    }
}
