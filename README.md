# PackageKicad

This project "pack" a Kicad project for a self containing form to be able to synchronize or distribute. Written in C# for .NET Core 2.1

Current version is v0.2Beta

Features (current and future):
- [x] Package symbol libraries into a project library
- [x] Linux compatibility (untested)
- [x] Proper command line handling
- [ ] generate .gitignore
- [ ] switchable backup
- [ ] option: copy libraries instead of package
- [ ] new library format handling
- [ ] package module libraries
- [ ] package 3D models
- [ ] symbol colision handling (versioning???)
- [x] code commenting
- [x] code license headers
- [ ] refactoring
- [ ] extend error handling
- [ ] logging
- [ ] validate only option
- [ ] OSX compatibility

License:
[This work is licensed under a GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
